import pandas as pd
import os

__root = '../data'

header_check_label_no2 = 'NO2'
header_check_label_so2 = 'SO2'
header_check_label_pm10 = 'PM10'
param_list = [[], []]


def get_header(df):
    index = 0
    exit_condition = False
    header = None
    while not exit_condition and index <= 50:
        row = df.iloc[index]
        row = row.tolist()

        result = header_check_label_no2 in row or \
                 header_check_label_so2 in row or \
                 header_check_label_pm10 in row or \
                 header_check_label_no2.lower() in row or \
                 header_check_label_so2.lower() in row or \
                 header_check_label_pm10.lower() in row
        if result:
            exit_condition = True
            header = row
        else:
            index += 1

    return header


def add_param_list(params):
    for element in params:
        element = element.upper()
        if element not in param_list[0]:
            param_list[0].append(element)
            param_list[1].append(1)
        else:
            param_list[1][param_list[0].index(element)] += 1


def get_params():
    error_sheets = 0
    valid_sheets = 0
    years = os.listdir(__root)
    for year in years:
        files = os.listdir(__root + '/' + year)
        for file in files:
            test_file = pd.ExcelFile(__root + '/' + year + '/' + file)
            sheets = test_file.sheet_names
            for sheet in sheets:
                print(__root + '/' + year + '/' + file + ' sheet_name: ' + sheet)
                df = test_file.parse(sheet, header=None)
                try:
                    header = get_header(df)
                    header.pop(0)
                    print(header)
                    add_param_list(header)
                    print('-----------')
                    valid_sheets += 1
                except:
                    print('ERRORE!')
                    print('-----------')
                    error_sheets += 1

    for element in param_list[0]:
        print('{number} {param}'.format(number=param_list[1][param_list[0].index(element)], param=element))

    print('-----------')
    print('ERROR: {err} - VALID: {valid}'.format(err=error_sheets, valid=valid_sheets))


if __name__ == '__main__':
    get_params()

